import { nodeResolve } from '@rollup/plugin-node-resolve'
import babel from '@rollup/plugin-babel'
import closure from '@ampproject/rollup-plugin-closure-compiler'

export default {
  input: 'src/index.ts',
  output: [
    {
      file: 'build/index.esm.js',
      format: 'es',
    },
    {
      file: 'build/index.js',
      format: 'cjs',
    },
  ],
  external: [/@babel\/runtime/],
  plugins: [
    nodeResolve({
      extensions: ['.js', '.jsx', '.ts', '.tsx'],
    }),
    babel({
      extensions: ['.js', '.jsx', '.ts', '.tsx'],
      babelHelpers: 'runtime',
      configFile: './.babelrc.js',
    }),
    closure({
      formatting: 'PRETTY_PRINT',
      language_out: 'ECMASCRIPT_NEXT',
    }),
  ],
}
