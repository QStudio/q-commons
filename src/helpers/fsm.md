# FSM

非常簡單的 FSM 機制。會將作為參數傳入的 object，透過事先定義的 `StateConfig` 進行加工、回傳。

```ts
interface FSM<K, D> {
  /**
   * @param data 要進行加工的 data
   * @param key 要從哪個 state 開始進行加工
   * @returns 加工完成的 data
   **/
  (data: D, from: K): D
}
```

## `generateFSM` 創建 FSM

```ts
interface ResultData<K, D> {
  to?: K | null
  data: D 
}

interface StateConfig<K, D> {
  stateKey: D

  onState(data: D): ResultData<K, D>
}

function generateFSM<K, D>(defaultState: K, stateList): FSM<K, D>
```