/**
 * @template K key type of every state
 * @template D data type of the  FSM
 * @version 0.5.0
 * @since 0.5.0
 */
interface ResultData<K, D> {
  /**
   * The key of next state.
   * If `null` or `undefined`, finish the FSM
   **/
  to?: K | null
  data: D
}

// eslint-disable-next-line @typescript-eslint/no-namespace
namespace ResultData {
  export function to<K, D>(stateKey: K, data: D): ResultData<K, D> {
    return {
      to: stateKey,
      data,
    }
  }

  export function finish<D>(data: D): ResultData<any, D> {
    return {
      data,
    }
  }
}

/**
 * @template K key type of every state
 * @template D data type of the  FSM
 * @version 0.5.0
 * @since 0.5.0
 */
interface StateConfig<K extends string | number, D> {
  key: K
  onState(data: D): ResultData<K, D>
}

/**
 * To process `data` through pre-defined state
 * @template K key type of every state
 * @template D data type of the  FSM
 * @version 0.5.0
 * @since 0.5.0
 */
interface FSM<K, D> {
  /**
   * @param data the data that would be processed.
   * @param state the start state.
   *   - If not provided, the round will start from pre-defined default state.
   * @returns the result data.
   */
  (data: D, state?: K): D
}

/**
 * To generate FSM
 * @template K key type of every state.
 * @template D data type of the  FSM.
 * @param defaultState the default start state of the FSM.
 * @param stateList the states of the FSM process through.
 * @version 0.5.0
 * @since 0.5.0
 */
function generateFSM<K extends string | number, D>(
  defaultState: K,
  stateList: StateConfig<K, D>[]
): FSM<K, D> {
  const stateMap: Record<K, StateConfig<K, D>> = {} as Record<
    K,
    StateConfig<K, D>
  >
  stateList.forEach((config) => {
    const { key } = config
    stateMap[key] = config
  })

  return (data: D, state: K = defaultState): D => {
    const track = []
    let s: K | null = state
    let d = data
    while (s !== null && stateMap[s]) {
      track.push({ state: s, data: d })
      const { onState } = stateMap[s] as StateConfig<K, D>
      const result = onState(d)
      s = result.to || null
      d = result.data
    }

    if (process.env.NODE_ENV !== 'production') {
      // console.log('FSM', track)
    }

    return d
  }
}

export { generateFSM, ResultData }
export type { FSM, StateConfig }
