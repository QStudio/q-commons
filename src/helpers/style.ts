import CSS from 'csstype'

// MARK: Class Selector Validator

/**
 * @private
 * @version 0.1.0
 * @since 0.1.0
 */
type ClassNameValue = string | boolean | undefined | null | Record<string, any>

/**
 * @private
 * @version 0.1.0
 * @since 0.1.0
 */
type ClassNameArgument = Array<ClassNameValue | ClassNameArgument>

/**
 * @private
 * @version 0.1.0
 * @since 0.1.0
 */
function joinRecordClassName(record: Record<string, any>): string {
  const result = []
  const arr = Object.entries(record)
  let j = 0
  while (j < arr.length) {
    const [selector, valid] = arr[j++]
    if (selector && valid) {
      result.push(selector)
    }
  }

  return result.join(' ')
}

/**
 * To join every css-class-selector in the classSelectors as a string value.
 * @version 0.1.0
 * @since 0.1.0
 * @example
 * const buttonClassName = classNames('q-button', {
 *   active: true
 * }) // 'q-button active'
 *
 * const dropdownClassName = classNames([
 *   'q-selector',
 *   ['padding--s', 'border--s'],
 *   {
 *     active: true
 *   }
 * ]) // 'q-selector padding--s border--s active'
 */
function classNames(
  ...selectors: (ClassNameValue | ClassNameArgument)[]
): string {
  let result = ''
  let i = 0
  while (i < selectors.length) {
    const selector = selectors[i++]
    if (selector) {
      if (Array.isArray(selector)) {
        result += ` ${classNames(...selector)}`
      } else if (typeof selector === 'object') {
        result += ` ${joinRecordClassName(selector)}`
      } else {
        result += ` ${selector}`
      }
    }
  }

  return result.trim()
}

// MARK: Rem Formattor

/**
 * To suffix the value with `rem` if typeof is `number`
 * @returns validated className string
 * @version 0.1.0
 * @since 0.1.0
 * @example
 * const borderWidth = rem(1) // '1rem'
 * const padding = rem('2%')  // '2%'
 */
function rem(value?: number | string | null): string {
  if (typeof value === 'undefined' || value === null) {
    return ''
  }
  return typeof value === 'number' ? `${value}rem` : value
}

// MARK: CSS Generator

const STYLE_CLASS_SET = new Set()

const STYLE_ROOT = document.createElement('style')
document.head.appendChild(STYLE_ROOT)

function generateSelector(property: string, value: string | number): string {
  return `${property}_${value.toString().replaceAll(' ', '-')}`
}

/**
 * To generate css class selector by css properties
 * @returns CSS class selector
 * @version 0.1.0
 * @since 0.1.0
 * @example
 * const buttonClassName = css({
 *   border: '1px solid grey',
 *   color: 'white'
 * }) // 'border_1px-solid-grey color_white'
 */
function css(
  props: CSS.Properties & Record<string, string | number | undefined | null>,
  selectorGenerator = generateSelector
): string {
  const classNameList: string[] = []
  const propEntries = Object.entries(props)
  if (!propEntries.length) {
    return ''
  }

  propEntries.forEach(([prop, value]) => {
    if (!value && typeof value !== 'number') {
      return
    }

    const className = selectorGenerator(prop, value)
    if (!STYLE_CLASS_SET.has(className)) {
      const rule = `.${className}{${prop}:${value};}`
      STYLE_ROOT.setAttribute('data-updated-time', Date.now().toString())
      if (STYLE_ROOT.sheet) {
        STYLE_ROOT.sheet.insertRule(rule)
      } else {
        STYLE_ROOT.innerText += rule
      }
      STYLE_CLASS_SET.add(className)
    }

    classNameList.push(className)
  })

  return classNameList.join(' ')
}

// MARK: Exports

export { classNames, rem, css }
export { STYLE_ROOT }
