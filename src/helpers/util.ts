/**
 * To generate random id
 * @example
 * const id = randomId() // '_abcd1234'
 */
function randomId(): string {
  const a = Math.random().toString(36).slice(2)
  const b = Date.now().toString(36).substr(0, 6)
  let result = '_'
  let i = 0
  while (i < a.length && i < b.length) {
    result += a[i]
    result += b[i]
    i++
  }

  result += a.slice(i)
  result += b.slice(i)

  return result
}

export { randomId }
