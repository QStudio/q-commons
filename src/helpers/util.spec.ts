import { randomId } from './util'

test('randomId', () => {
  expect(randomId()).toBeTruthy()
  expect(randomId()).not.toBe(randomId())
})
