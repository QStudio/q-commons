import { classNames, css, rem } from './style'

test('test `classNames`', () => {
  expect(classNames('a', 'b', 'c')).toBe('a b c')
  expect(classNames(['a', 'b', 'c'])).toBe('a b c')
  expect(classNames({ a: true, b: false, c: true })).toBe('a c')
  expect(classNames('a', ['b', { c: true, d: false }, ['e', 'f']])).toBe(
    'a b c e f'
  )
})

test('test `rem`', () => {
  expect(rem()).toBe('')
  expect(rem(123)).toBe('123rem')
  expect(rem('123rem')).toBe('123rem')
})

test('test `css`', () => {
  expect(css({})).toBe('')
  expect(css({ color: undefined })).toBe('')

  const props = { color: 'red' }
  expect(css(props)).toBe('color_red')

  const t = (prop: string, value: string | number) => `${prop}_${value}_test`
  expect(css(props, t)).toBe('color_red_test')
})
