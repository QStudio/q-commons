const fs = require('fs')

const CJS_FILE_NAME = 'index.js'
const EMS_FILE_NAME = 'index.esm.js'
const ENV_VARIABLE_LIST = [
  'CI_PROJECT_ROOT_NAMESPACE',
  'CI_PROJECT_NAME',
  'CI_COMMIT_TAG',
  'CI_SERVER_PROTOCOL',
  'PACKAGE_PUBLISH_DOMAIN',
]

function prepareEnvironment(env = {}) {
  const result = {}
  ENV_VARIABLE_LIST.forEach((varName) => {
    result[varName] = env[varName] || ''
  })

  return result
}

async function preparePackage(rawEnv = {}) {
  const env = prepareEnvironment(rawEnv)
  const p = JSON.parse(await fs.promises.readFile('./package.json'), 'utf-8')
  p.private = false
  p.type = 'module'
  p.main = `./${CJS_FILE_NAME}`
  p.module = `./${EMS_FILE_NAME}`
  p.exports = {
    import: `./${EMS_FILE_NAME}`,
    require: `./${CJS_FILE_NAME}`,
  }

  const [, version] = /^v(.*)/.exec(env.CI_COMMIT_TAG) || []
  p.version = version || p.version
  p.scripts = {}

  let scope = env.CI_PROJECT_ROOT_NAMESPACE
  if (scope) {
    scope = `@${scope}`
    const packageName = env.CI_PROJECT_NAME.toLowerCase()
    if (packageName) {
      p.name = `${scope}/${packageName}`
    }

    if (env.CI_SERVER_PROTOCOL && env.PACKAGE_PUBLISH_DOMAIN) {
      p.publishConfig = {
        [`${scope}:registry`]: `${env.CI_SERVER_PROTOCOL}://${env.PACKAGE_PUBLISH_DOMAIN}`,
      }
    }
  }

  fs.promises.writeFile('./build/package.json', JSON.stringify(p, null, '  '))
}

preparePackage(process.env)
